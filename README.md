# **PETUNIA-POT SCRIPT**
This script is made to help you checking your 42 project or others project,
it check if you got valid makefile, author file (if there is one), header
and some Norms Rules from the PDF.
  
# **DISCLAIMER**
Every output of this script is a Warning, you have to check the founded error
before punishing with a *0* or a *flag* on a project correction.
If you use it as the only true/standard for doing correction,
that's mean you're a *DICKHEAD*.
Of course, *DICKHEAD*, are not allowed to use this script.
  
# **LICENSE**
Do whatever you want with this script,
this is my gift to the 42 world, copy, modify. Just use it, for a greater good.
Just tell to the other, that you took it from a simple guy named clrichar.
  
# **THANK**
I would like to thank all people that have written Unit-test before me,
I've been very inspirated by your work, some of them:  
 * jgigault  
 * ataguiro
  

I would also thank some good friends, or mate that help me to go through:  
 * hmassonn  
 * kehuang  
 * lmeyre  
 * ndelest  
 * rbadia  
 * gduval  
 * prossi  
  
## Some Notes for sh_utils
For malloc.py use it like that -> python2.7 malloc.py [...file]  
Eternal Leaks conspiration -> while [1]; leaks [...name of bin]; done;
